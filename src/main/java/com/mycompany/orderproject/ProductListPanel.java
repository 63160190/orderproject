/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.orderproject;

import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class ProductListPanel extends javax.swing.JPanel implements OnBuyProductListener{
    
    private ArrayList<OnBuyProductListener> subcirberList = new ArrayList<>();
    private final ArrayList<Product> ProductList;
    private OrderPanel orderPanel;

    /**
     * Creates new form ProductListPanel
     */
    public ProductListPanel() {
        initComponents();
        ProductList = Product.getMockProductList();
        for(Product product:ProductList){
            ProductPanel panel = new ProductPanel(product);
            productPanel.add(panel);
            panel.addOnBuyListenner(this);
           
            
        }
        int num = productPanel.getComponentCount();
        int col =3;
        productPanel.setLayout(new GridLayout(num/col + (num%col>0?1:0),col));    
    }
    public void addOnBuyListener(OnBuyProductListener subcirber){
        subcirberList.add(subcirber);
    }
    public void setOrderPanel(OrderPanel orderpanel){
        this.orderPanel = orderPanel;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        productPanel = new javax.swing.JPanel();

        javax.swing.GroupLayout productPanelLayout = new javax.swing.GroupLayout(productPanel);
        productPanel.setLayout(productPanelLayout);
        productPanelLayout.setHorizontalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 542, Short.MAX_VALUE)
        );
        productPanelLayout.setVerticalGroup(
            productPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 462, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(productPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel productPanel;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Product product, int amount) {
        //orderPanel.buy(product, amount);
        for(OnBuyProductListener s:subcirberList){
            s.buy(product,amount);
        }
    }
}
